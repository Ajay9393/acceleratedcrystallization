import pymatgen
from ase import Atoms
from ase.io import read,write
from pymatgen.io.ase import AseAtomsAdaptor
from pymatgen.io.vasp import Poscar, Incar, Kpoints, Potcar, Kpoints, Vasprun
from pymatgen.io.vasp.sets import MPRelaxSet
import os
import subprocess
import pandas as pd
import numpy as np
import ase.build
from ase import Atoms
from pymatgen.ext.matproj import MPRester
from pymatgen.io.vasp import Vasprun
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer
from pymatgen.apps.borg.hive import VaspToComputedEntryDrone
from pymatgen.apps.borg.queen import BorgQueen
from pymatgen.entries.compatibility import MaterialsProjectCompatibility
from pymatgen.io.vasp import Vasprun
from itertools import product


folders = []
a_atoms = []
dic_oxistates = {'Cd':2, 'Pb':2, 'Sn':2,'Ge':2, 'Cl':-1, 'Br':-1, 'I':-1, 'Cs':1,'Rb':1}
cell = [[15,0,0],[0,15,0],[0,0,15]]

for key in dic_oxistates.keys():
    print(key)
    a = Atoms(key, cell = cell,pbc=True)
    struc = AseAtomsAdaptor.get_structure(a)
    if not os.path.exists(key):
        os.system('mkdir {0}'.format(key))
    #oxistates = [dic_oxistates[i] for i in  a.get_chemical_symbols()]
    #struc.add_oxidation_state_by_site(oxistates)
    struc.add_oxidation_state_by_element(dic_oxistates)
    struc._charge = struc.charge
    relax = MPRelaxSet(struc)
    relax.user_incar_settings={'ISPIN':1,'IBRION':-1,'ISMEAR':0,'SIGMA':0.05,  'NPAR':8, 'POTIM':0.05, 'ALGO':'All','NSW':0 }
    relax.write_input(key)
    folders.append(key)
    a_atoms.append(a)

dict_atoms = { 'MA':  Atoms(['N', 'C', 'H', 'H', 'H', 'H', 'H', 'H'],cell = cell, pbc = True, 
             positions =np.array( [[5.62941456, 5.62940118, 5.62941065],
                                   [6.46972906, 6.46971567, 6.46972514],
                                   [5.26032037, 6.1395259 , 4.8038877 ],
                                   [4.81155173, 5.3565966 , 6.23556137],
                                   [6.14067666, 4.79864048, 5.2542783 ],
                                   [6.48560958, 6.09916788, 7.510144  ],
                                   [6.01346572, 7.48404625, 6.44047274],
                                   [7.50337432, 6.54232246, 6.11022013]])),
               'FA': Atoms( ['N', 'C', 'N', 'H', 'H', 'H', 'H', 'H'], cell = cell, pbc = True,
              positions= np.array([[5.6379013 , 5.63788791, 5.63789738],
                                   [6.45983198, 6.4598186 , 6.45982806],
                                   [6.13108069, 6.81551084, 7.64464878],
                                   [6.10295818, 5.05369909, 4.91669552],
                                   [4.60836093, 5.58840236, 5.71637491],
                                   [7.40950735, 6.81250902, 6.08943137],
                                   [5.21420493, 6.47416445, 8.00065452],
                                   [6.76095865, 7.41129907, 8.19929902]])) }
for key in dict_atoms.keys():
    print(key)
    a = dict_atoms[key]
    struc = AseAtomsAdaptor.get_structure(a)
    if not os.path.exists(key):
        os.system('mkdir {0}'.format(key))
    struc._charge = 1
    relax = MPRelaxSet(struc)
    relax.user_incar_settings={'ISPIN':1,'IBRION':2,'ISMEAR':0,'SIGMA':0.05,  'NPAR':8, 'POTIM':0.05, 'ALGO':'All','NSW':99 }
    relax.write_input(key)
    folders.append(key)
    a_atoms.append(a)

