#####################################################################################################
# Robotic crystallization code
# By: Mikhail Askerka, Jeffrey Kirman
#####################################################################################################

#####################################################################################################
# Imports
#########

from __future__ import print_function
import os
#os.environ["CUDA_VISIBLE_DEVICES"] = "3"
import time
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import metrics
import tensorflow as tf
from itertools import product
from scipy.interpolate import griddata
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler,StandardScaler
import h5py
import pickle as pkl
from collections import Counter
from itertools import product
import keras
import numpy as np
import tensorflow as tf
from keras.models import Sequential, Model
from keras.utils import plot_model
from keras.layers import Lambda,Dense, Dropout, Activation, Flatten, Input, Lambda, Conv2D, MaxPooling2D
from keras.callbacks import ModelCheckpoint
from keras.regularizers import l2
from keras.utils import multi_gpu_model
import data_and_calcs as dac
import json
import shutil
import imageio
import sys

#######################################################################################################
# Variables and Setup
#####################

# User changeable variables

nstart=6
regularization=0.001
repeat_filts=6
dropout=0.2
activ='relu'
add_reductive=True
nfilters=8
filt_size=(3,3)
lr=0.00699027
model_name='m_my_test_model'

exp_name='YG-2019-02-27' # Name of experiment data for saving the file

all_nns_dir = 'trained_nets/'
#nn_dir = 'trained_nets/saved_models_Gap_GGA_SOC_3225_32_200/mydb/motif_initialization/train_models/'
#nn_path= 'trained_nets/saved_models_Gap_GGA_SOC_3225_32_200/mydb/motif_initialization/train_models/0d5a30f3-ad6e-4404-84f1-d18b459d7190'
#data_path = '/scratch/e/esargent/kirmanje/12It52Xi99YAU/compiled_array.npy'
#target_path = '/scratch/e/esargent/kirmanje/12It52Xi99YAU/labels_array-2.npy'

data_path = ['/ess01/scratch2/e/esargent/kirmanje/Experiments/2018-10-02-1/image_data.npy', '/ess01/scratch2/e/esargent/kirmanje/Experiments/2018-10-02-2/image_data.npy']
meta_data_path = ['/scratch/e/esargent/kirmanje/Experiments/2018-10-02-1/image_meta_data.pickle','/scratch/e/esargent/kirmanje/Experiments/2018-10-02-2/image_meta_data.pickle']
target_path = ['/ess01/scratch2/e/esargent/kirmanje/Experiments/2018-10-02-1/labels.npy', '/ess01/scratch2/e/esargent/kirmanje/Experiments/2018-10-02-2/labels.npy']

new_data_path = '/scratch/e/esargent/kirmanje/Training/40000.pkl'

over_sample = True
patience = 10

mode = 'test'

# Setup tensorflow
config = tf.ConfigProto()
config.gpu_options.allow_growth=True
config.gpu_options.allocator_type ='BFC'
config.gpu_options.per_process_gpu_memory_fraction = 0.8

# Prepare data
#data = dac.prepare_data(data_path, target_path, over_sample)
#data_fft = dac.prepare_data(data_path, target_path, over_sample, fft=True)
setup_model = dac.setup_model_functional
########################################################################################################
# Build neural net and run
##########################

# Run a single neural network
# data: data to be inputed to the NN either partitioned into train, val, test for test mode, or no partitioning for regular NN run
# nn_dir, nn_name: strings with the NN directory and name to locate the JSON file
# test: if TRUE will run the test partition of a dataset to an already trained NN, if FALSE will run a full dataset
# save: set to TRUE to save the output of the NN
# ext_data: set to TRUE if no preprocessing (gs and ft) is needed for the data given, FALSE if preprocessing is needed
# save_dir: string of path where the output of the NN will be saved
def run_nn(data, nn_dir, nn_name, test=True, save=True, ext_data=False, save_dir=''):
    
    # Load all the NN parameters from the JSON file
    json_file = open(nn_dir + nn_name + '.json', 'r')
    loaded_model_json = json.loads(json_file.read())
    json_file.close()
    m_json = loaded_model_json['param']['model']
    nstart = m_json['nstart']
    regularization = m_json['regularization']
    repeat_filts = m_json['repeat_filts']
    dropout = m_json['dropout']
    activ = m_json['activ']
    add_reductive = m_json['add_reductive']
    nfilters = m_json['nfilters']
    filt_size = tuple(m_json['filt_size'])
    lr = m_json['lr']
    model_name = m_json['model_name']
    print(loaded_model_json)
    try:
        over_sample = loaded_model_json['param']['data']['over_sample']
    except:
        over_sample = False
    try:
        data_paths = loaded_model_json['param']['data']['data_paths']
    except:
        data_paths = ''
    try:
        target_paths = loaded_model_json['param']['data']['target_paths']
    except:
        target_paths = ''
    try:
        fft = loaded_model_json['param']['data']['fft']
    except:
        fft = False
    print('FFT: ' + str(fft))
    try:
        dense_activ = m_json['dense_activ']
    except:
        dense_activ = None

    try:
        loss = m_json['loss']
    except:
        loss = 'mse'
    
    try:
        by_well=loaded_model_json['param']['data']['by_well']
    except:
        by_well=False

    try:
        trans_rot=loaded_model_json['param']['data']['trans_rot']
    except:
        trans_rot=False

    try:
        trans_px=loaded_model_json['param']['data']['trans_px']
    except:
        trans_px=2

    try:
        pre_concat=m_json['pre_concat']
    except:
        pre_concat=10
   
    try:
        gs = loaded_model_json['param']['data']['gs']
    except:
        gs = False

    if test:
        if data == None:
            data = dac.prepare_data(data_paths, target_paths, over_sample, by_well=by_well, trans_rot=trans_rot, trans_px=trans_px, fft=fft, gs=gs)
        model = setup_model(data[0],activ,dense_activ,nstart,repeat_filts,filt_size,add_reductive,nfilters,dropout,regularization,loss,model_name,lr,fft,pre_concat)
        model.load_weights((nn_dir + nn_name + '.h5').format(model_name))
 
        (x_train, y_train), (x_val, y_val), (x_test, y_test) = data[0], data[1], data[2]
        data_screen = x_test 
        target = model.predict(data_screen)
        if save:
            np.save(nn_dir + model_name + '_test.npy', target)
   
        # Calculate scores
        target_bool = np.greater(target, 0.5)
        y_test_bool = np.greater(y_test, 0.5)
        confusion_matrix = metrics.confusion_matrix(y_test_bool, target_bool)
        f1 = metrics.f1_score(y_test_bool, target_bool)
        precision = metrics.precision_score(y_test_bool, target_bool)
        recall = metrics.recall_score(y_test_bool, target_bool)
        
        return (f1, precision, recall, confusion_matrix, target)
    else:
        if not ext_data:
            if gs:
                data = np.average(data / 255., axis=-1)[...,np.newaxis]
            if fft:
                data_batch_gs = np.average(data / 255., axis=-1)
                data_batch_fft = np.fft.fft2(data_batch_gs)
                data_batch_fft_mag = np.absolute(data_batch_fft)[...,np.newaxis]
                data_batch_fft_ang = np.angle(data_batch_fft)[...,np.newaxis]
                data = np.concatenate((data, data_batch_fft_mag, data_batch_fft_ang), axis=-1)

        model = setup_model([data],activ,dense_activ,nstart,repeat_filts,filt_size,add_reductive,nfilters,dropout,regularization,loss,model_name,lr,fft,pre_concat)
        try:
            model.load_weights((nn_dir + nn_name + '.h5').format(model_name))
        except OSError:
            model.load_weights((nn_dir + nn_name + '.hdf5').format(model_name))
        data_screen = data
        target = model.predict(data_screen)
        if save:
            np.save(save_dir + model_name + '_' + exp_name +'.npy', target)
        return target

# Runs all NNs in a specified folder in test mode
# Returns a list of calculated F1 scores, precision, recall and confusion matrices for all NN ran 
def run_all_nns(data, nns_path, test):
    files = os.listdir(nns_path)
    nn_names = []
    f1 = []
    prec = []
    rec = []
    for f in files:
        if f.endswith('.h5'):
            nn_name = f.rsplit('.')[0]
            if nn_name + '.json' in files:
                (f1_temp,prec_temp,rec_temp,confusion_matrix,targets)=run_nn(data, nns_path, nn_name, test)
                f1.append(f1_temp)
                prec.append(prec_temp)
                rec.append(rec_temp)
                nn_names.append(nn_name)

    scores = np.array((f1, prec, rec))
    max_f1_score_idx = np.argmax(scores[0])
    return scores[:,max_f1_score_idx], max_f1_score_idx, scores, nn_names

#(max_score, max_score_idx, scores, nn_names) = run_all_nns(dac.prepare_data(data_path, target_path, over_sample), nn_dir) 

def generate_scores_all_nns(data=None, test=True, overwrite=False, preprocessing=False):
    all_nns = os.listdir(all_nns_dir)
    for nn in all_nns:
        print(nn)  
        if preprocessing:
            with open(all_nns_dir + nn + '/data_path.json') as json_file:  
                data_info = json.load(json_file)
                data_path = data_info['data_path']
                try:
                    fft = data_info['fft']
                    gs = data_info['gs']
                except:
                    fft = False
                    gs = False
            f = open(data_path, 'rb')
            if sys.version_info[0] > 2:
                data = pkl.load(f, encoding='latin1')
            else:
                data = pkl.load(f)
        else:
            data = None
        if (('scores.json' not in os.listdir(all_nns_dir + nn)) or overwrite) and len(os.listdir(all_nns_dir + nn + '/mydb/motif_initialization/train_models/')) > 2:
            if preprocessing:
                data = dac.prepare_data2(data, fft=fft, gs=gs)
            (max_score, max_score_idx, scores, nn_names) = run_all_nns(data, all_nns_dir + nn + '/mydb/motif_initialization/train_models/', test)
            scores_dict = {'max_score': max_score.tolist(), 'max_score_idx': int(max_score_idx), 'f1': scores[0].tolist(), 'precision': scores[1].tolist(), 'recall': scores[2].tolist(), 'names': nn_names}
            print(nn + ': ' + np.array_str(max_score))
            with open(all_nns_dir + nn + '/scores.json', 'w') as outfile:
                json.dump(scores_dict, outfile)

def read_best_scores_from_jsons(all_nns_dir):
    dirs = os.listdir(all_nns_dir)
    for nn in dirs:
        files = os.listdir(all_nns_dir + nn + '/')
        for f in files:
            if 'scores.json' in f:
                with open(all_nns_dir + nn + '/scores.json', 'rb') as outfile:
                    scores_dict = json.loads(outfile.read())
                print(nn)
                print(scores_dict['names'][scores_dict['max_score_idx']] + ': ' + str(scores_dict['max_score']))
                print()

def read_all_scores_from_json(json_path):
    with open(json_path, 'r') as outfile:
        scores_dict = json.loads(outfile.read())
    del scores_dict['max_score']
    del scores_dict['max_score_idx']
    df = pd.DataFrame(scores_dict)
    print(df)

def label_binary_from_output(output, confidence=0.75, activ='sigmoid'):
    if activ=='sigmoid':
        labels = 1/(1+np.exp(-output))
    else:
        labels = output

    return labels > confidence

def save_images(data, save_dir):
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    for c, im in enumerate(data):
        imageio.imwrite(save_dir+str(c)+'.jpg', im)

def copy_images_from_folder(images_dir, save_dir, targets):
    imlist = os.listdir(images_dir)
    imlist.sort()
    images = np.asarray(imlist)
    images_to_copy = images[targets.reshape(-1)]
    print(images_to_copy.shape)
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    for im in images_to_copy:
        shutil.copyfile(images_dir + im, save_dir + im)

#generate_scores_all_nns(dac.prepare_data(data_path, target_path, over_sample), overwrite=True)
