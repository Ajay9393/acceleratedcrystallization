# -*- coding: utf-8 -*-
"""
Created on Sat Apr 27 16:44:30 2019

@author: Jeffrey
"""

# Data manipulation imports
import numpy as np
import pickle as pkl
import pandas as pd
import sys
import cv2


def train_val_test_split_locater(set_size, val_frac, test_frac):
    train_test_split = int((1-test_frac)*set_size)
    train_val_split = int((1-val_frac)*train_test_split)
    return (train_test_split, train_val_split)


# This function splits the splittable parameter into train, val, and test sets
#   it then creates an index boolean by checking instances in the data_map parameter
#   which indexes the data into train, val, and test sets
def train_val_test_splitter(data, data_map, splittable, train_test_split, train_val_split):
    train_map = splittable[:train_val_split]
    val_map = splittable[train_val_split:train_test_split]
    test_map = splittable[train_test_split:]
    
    train = data[np.isin(data_map, train_map)]
    val = data[np.isin(data_map, val_map)]
    test = data[np.isin(data_map, test_map)]
    return (train, val, test)

def RGB_HSV(ims, rgb_to_hsv=True, rgb=True):
    if rgb_to_hsv:
        if rgb:
            colour_code = cv2.COLOR_RGB2HSV
        else:
            colour_code = cv2.COLOR_BGR2HSV
    else:
        if rgb:
            colour_code = cv2.COLOR_HSV2RGB
        else:
            colour_code = cv2.COLOR_HSV2BGR
    conv_ims = []
    for im in ims:
        conv_ims.append(cv2.cvtColor(src=im,code=colour_code))
    return np.stack(conv_ims, axis=0)

# This function takes images and randomly applies colour noise to them (brightness, hue, saturation)
def colour_noise_randomizer(ims, labels, amount_oversampled, seed=42):
    hsv_ims = RGB_HSV(ims, rgb_to_hsv=True)
    np.random.seed(seed)
    idxs = np.random.randint(ims.shape[0], size=amount_oversampled)
    oversampled_ims = hsv_ims[idxs]
    oversampled_lbls = labels[idxs]
    
    hue_mod = np.round((np.random.rand(oversampled_ims.shape[0]) - 0.5) * 140)
    sat_mod = np.random.rand(oversampled_ims.shape[0]) + 0.5
    val_mod = np.round((np.random.rand(oversampled_ims.shape[0]) - 0.5) * 64)
    
    oversampled_ims[...,0] = np.clip(oversampled_ims[...,0] + hue_mod[:,np.newaxis,np.newaxis],0,180)
    oversampled_ims[...,1] = np.clip(oversampled_ims[...,1] * sat_mod[:,np.newaxis,np.newaxis],0,255)
    oversampled_ims[...,2] = np.clip(oversampled_ims[...,2] + val_mod[:,np.newaxis,np.newaxis] * (oversampled_ims[...,2] > 0).astype(np.uint8),0,255)
    
    return (RGB_HSV(oversampled_ims, rgb_to_hsv=False), oversampled_lbls)

def white_noise_randomizer(ims, labels, amount_oversampled, seed=42):
    np.random.seed(seed)
    idxs = np.random.randint(ims.shape[0], size=amount_oversampled)
    noised_lbls = labels[idxs]

    mod = np.round((np.random.rand(idxs.shape[0],ims.shape[1],ims.shape[2],ims.shape[3]) - 0.5) * 20)

    noised_ims = np.clip(ims[idxs] + mod * (np.sum(ims[idxs],axis=-1,keepdims=True) > 0).astype(np.uint8),0,255)    
    
    return (noised_ims.astype(np.uint8), noised_lbls)
    
def rotate_image(image, angle):
    image_center = tuple(np.array(image.shape[1::-1]) / 2)
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
    return result

def rotate_images(images, angles):
    rotations = []
    for im, ang in zip(images, angles):
        rotations.append(rotate_image(im, ang))
    return np.stack(rotations)
        
def rotation_randomizer(ims, labels, amount_oversampled, angle_step=90, seed=42):
    np.random.seed(seed)
    idxs = np.random.randint(ims.shape[0], size=amount_oversampled)
    rotated_lbls = labels[idxs]
    if angle_step<=0 or angle_step > 360:
        angles = np.random.rand(idxs.shape[0]) * 360
    else:
        angles = np.random.randint(1,int(360/angle_step), size=idxs.shape[0]) * angle_step
    rotated_ims = rotate_images(ims[idxs],angles)
    
    return (rotated_ims, rotated_lbls)
    
# This function splits a repository of images into training, validation, and test sets
def generate_datasets_by_well(im_paths, meta_data_paths, label_paths, val_frac=0.2, test_frac=0.1, seed=42):
    
    data = [[],[],[]]
    labels = [[],[],[]]
    
    for im_path, md_path, lbl_path in zip(im_paths, meta_data_paths, label_paths):
       
        print(im_path)

        # Load images and labels
        ims = np.load(im_path)
        lbls = np.load(lbl_path)
        
        # Load meta data and extract well data
        f = open(md_path, 'rb')
        if sys.version_info[0] > 2:
            md = pkl.load(f, encoding='latin1')
        else:
            md = pkl.load(f)
        wells_drops = md.loc[:,['Well','Drop']].values
        wd_ids = wells_drops[:,0] + wells_drops[:,1]*np.max(wells_drops[:,0])
        
        # Create datafrome containing [Index, Well-Drop ID#, Label]
        df = pd.DataFrame(np.stack([wd_ids, lbls], axis=1), columns=['Well-Drop ID','Labels'])
        
        # Sort the dataset by labels then parse it out so if a positive label in any well-drop it is classified as so
        df.sort_values('Labels', inplace=True)
        df.drop_duplicates(subset='Well-Drop ID', keep='last', inplace=True)
        
        # Seperate the positive and negative well-drop IDs then split them into respective datasets
        pos_lbl_idxs = np.argwhere(df['Labels'].values)
        pos_wd_ids = df['Well-Drop ID'].values[pos_lbl_idxs]
        neg_lbl_idxs = np.argwhere(df['Labels'].values == 0)
        neg_wd_ids = df['Well-Drop ID'].values[neg_lbl_idxs]
        
        pos_tt_split, pos_tv_split = train_val_test_split_locater(pos_wd_ids.shape[0], val_frac, test_frac)
        p_ims = [[],[],[]]
        p_lbls = [[],[],[]]
        p_ims[0], p_ims[1], p_ims[2] = train_val_test_splitter(ims, wd_ids, pos_wd_ids, pos_tt_split, pos_tv_split)
        p_lbls[0], p_lbls[1], p_lbls[2] = train_val_test_splitter(lbls, wd_ids, pos_wd_ids, pos_tt_split, pos_tv_split)
        
        neg_tt_split, neg_tv_split = train_val_test_split_locater(neg_wd_ids.shape[0], val_frac, test_frac)
        n_ims = [[],[],[]]
        n_lbls = [[],[],[]]
        n_ims[0], n_ims[1], n_ims[2] = train_val_test_splitter(ims, wd_ids, neg_wd_ids, neg_tt_split, neg_tv_split)
        n_lbls[0], n_lbls[1], n_lbls[2] = train_val_test_splitter(lbls, wd_ids, neg_wd_ids, neg_tt_split, neg_tv_split)
            
        for i in range(len(p_ims)):
            data[i].append(np.concatenate((p_ims[i],n_ims[i]), axis=0))
            labels[i].append(np.concatenate((p_lbls[i],n_lbls[i]), axis=0))
        
    return ((np.concatenate(data[0], axis=0), np.concatenate(labels[0], axis=0)),
            (np.concatenate(data[1], axis=0), np.concatenate(labels[1], axis=0)),
            (np.concatenate(data[2], axis=0), np.concatenate(labels[2], axis=0)))
    

# Preprocess images for the CNN
# im_paths, meta_data_paths, label_paths: directories
# total_oversampling: the number of desired images in the entire dataset
# save_path: save path for a pickle file to contain the preprocessed images
def preprocess(im_paths, meta_data_paths, label_paths, total_oversampling, val_frac=0.2, test_frac=0.1, seed=42, save_path=''):
    
    data_os = [[],[],[]]
    labels_os = [[],[],[]]
    
    datasets = generate_datasets_by_well(im_paths, meta_data_paths, label_paths, val_frac=val_frac, test_frac=test_frac, seed=seed)
    
    os = []
    os.append(max(total_oversampling*(1-test_frac)*(1-val_frac)-datasets[0][0].shape[0],0))
    os.append(max(total_oversampling*(1-test_frac)*(val_frac)-datasets[1][0].shape[0],0))
    os.append(max(total_oversampling*test_frac-datasets[2][0].shape[0],0))
    
    # Oversample the positive and negative cases seperately
    for i in range(len(datasets)):
        ims = [datasets[i][0]]
        lbls = [datasets[i][1]]
        
        idxs = []
        idxs.append(np.argwhere(datasets[i][1])[:,0])
        idxs.append(np.argwhere(datasets[i][1]==0)[:,0])
        
        if os[0] > 0:
            os2 = os.copy()
            os2[i] = [int(os[i]*(idxs[1].shape[0]/datasets[i][1].shape[0])), int(os[i]*(idxs[0].shape[0]/datasets[i][1].shape[0]))]
        
            for j in range(len(idxs)):
                ims_cn, lbls_cn = colour_noise_randomizer(datasets[i][0][idxs[j]], datasets[i][1][idxs[j]], int(os2[i][j]/2), seed=43)
                ims.append(ims_cn)
                lbls.append(lbls_cn)
                
                ims_rot, lbls_rot = rotation_randomizer(datasets[i][0][idxs[j]], datasets[i][1][idxs[j]], int(os2[i][j]/2), seed=44)
                ims.append(ims_rot)
                lbls.append(lbls_rot)
            
        data_os[i] = np.concatenate(ims)
        labels_os[i] = np.concatenate(lbls)
    
    preprocessed = tuple(zip(data_os, labels_os))
    
    if not(save_path==''):
        pkl.dump(preprocessed, open(save_path , "wb"))
    
    return preprocessed
