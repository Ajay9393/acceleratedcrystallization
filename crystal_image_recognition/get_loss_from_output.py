import numpy as np
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt

with open('output_CNN-2_a.out', 'r') as file:
    content = file.read().replace('\n', '')


loss = []
val_loss=[]
for i in range(0,len(content)-1):
	if content[i-3:i+7]=='p - loss: ':
		temp = content[i+7:i+13]
		if content[i+13] == 'e':
			temp = temp + content[i+13:i+17]
		loss.append(float(temp))
        if content[i:i+8] == 'val_loss':
		temp = content[i+10:i+16]
		val_loss.append(float(temp))
loss = np.asarray(loss)
val_loss = np.asarray(val_loss)
all_loss = np.stack((loss,val_loss))
epoch = np.arange(1,len(loss)+1)

fig = plt.figure(figsize=(8, 6), dpi=80)
    
ax = fig.add_subplot(111)
fig.subplots_adjust(top=0.85, left = .15, right = .85, bottom = .15)
   
ax.set_xlabel('Epoch')
ax.set_ylabel('Loss')

ax.plot(epoch, all_loss.T)
ax.legend(['training loss', 'validation loss'], loc='upper right', prop={'size': 8})
fig.savefig("loss_cnn2a.pdf")
