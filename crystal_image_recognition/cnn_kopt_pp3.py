#####################################################################################################
# Imports
#########

from __future__ import print_function
import tensorflow as tf
import keras
import numpy as np
from sklearn import metrics
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, Conv2D
from keras.callbacks import ModelCheckpoint
from keras.regularizers import l2
import data_and_calcs as dac
from hyperopt import hp
import sys
import argparse
import datetime as dt
import pickle as pkl


#######################################################################################################
# Variables and Setup
#####################

# Data path
data_path = '/scratch/e/esargent/kirmanje/Training/60000-best-consolidated.pkl'

# Model name
model_name= 'b32_preprocessed_fft4_mse_adam_f1'

# Hyperparameters
repeat_filts = hp.choice("m_repeat_filts", (1,2,3))
lr = hp.loguniform("m_lr", np.log(1e-4), np.log(1e-2))
batch_size = 32
batch_size_val = 64
dense_activ = None
activ = hp.choice("m_activ", ('relu','selu'))
add_reductive = True
filt_size = hp.choice("m_filt_size", ((2,2), (3,3), (4,4)))
epochs = 300
mode = 'train'
nstart = hp.choice("m_nstart", (3, 4, 5, 6))
nfilters = hp.choice("m_nfilters", (4, 8, 16, 32))
dropout = hp.uniform("m_dropout", 0.0, 0.4)
regularization = hp.loguniform("m_regularization", np.log(1e-4), np.log(1e-2))
loss = 'mse'
fft = True
pre_concat = 4
gs = False
optimizer = 'adam'
patience = 10

print('Model Name: ' + model_name)

# Prepare data
f = open(data_path, 'rb')
if sys.version_info[0] > 2:
    data = pkl.load(f, encoding='latin1')
else:
    data = pkl.load(f)

def data_fn(data_path, fft, gs):
    return dac.prepare_data2(data, fft=fft, gs=gs)

########################################################################################################
# KOPT
######
from hyperopt import fmin, tpe, hp, Trials
hyper_params = {
    "data":{
        "data_path": data_path,
        "gs": gs,
        "fft": fft,
        },
    "model": {
        "repeat_filts": repeat_filts,
        "lr": lr,
        "activ": activ,
        "dense_activ": dense_activ,
        "add_reductive": add_reductive,
        "filt_size": filt_size,
        "nstart": nstart,
        "nfilters": nfilters,
        "dropout": dropout,
        "regularization": regularization,
        "loss": loss,
        "model_name": model_name,
        "optimizer": optimizer,
        "fft": fft,
        "pre_concat": pre_concat,
        },
    "fit": {
        "epochs": epochs,
        "patience": patience,
        "batch_size": batch_size,
        "batch_size_val": batch_size_val
    }
}

from kopt import CompileFN
objective = CompileFN(db_name="mydb", exp_name="motif_initialization",  # experiment name
    data_fn = data_fn,
    model_fn = dac.setup_model_functional, 
    add_eval_metrics = ["mse", dac.f1], # metrics from concise.eval_metrics, you can also use your own
    optim_metric = "f1", # which metric to optimize for
    optim_metric_mode = "max", # maximum should be searched for
    valid_split = None, # use valid from the data function
    save_model = 'best', # checkpoint the best model
    save_results = True, # save the results as .json (in addition to mongoDB)
    save_dir = "./trained_nets/{1}_model_{0}/".format(model_name,dt.datetime.now().strftime('%Y-%m-%d--%H%M')))  # place to store the models
trials = Trials()
best = fmin(objective, hyper_params, trials=trials, algo=tpe.suggest, max_evals=100)
