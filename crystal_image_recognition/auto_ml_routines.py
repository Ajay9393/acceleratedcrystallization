import pandas as pd
from sklearn.model_selection import train_test_split
from tpot import TPOTRegressor
from sklearn.preprocessing import Imputer
import pickle
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import mean_absolute_error, r2_score


def one_hot_encode_string_and_impute(df_cleaned):
    for key in df_cleaned.dtypes[(df_cleaned.dtypes=='float')|(df_cleaned.dtypes=='int')].index:
        df_cleaned[key] = df_cleaned[key].fillna(df_cleaned[key].median())
    df_cleaned_transformed = df_cleaned.copy()
    for key in df_cleaned.dtypes[df_cleaned.dtypes=='object'].index:
        df_cleaned[key] = df_cleaned[key].fillna(df_cleaned[key].value_counts().index[0])
        df_temp =  pd.get_dummies(df_cleaned[key])
        df_cleaned_transformed.drop(key, axis=1,inplace=True)
        df_cleaned_transformed[df_temp.keys()] = df_temp
    return df_cleaned_transformed

def auto_ML_prepare_data(df, target,unimportant,cannot_be_influenced_directly,correlated,not_a_target):
    df_cleaned = df[df[target].isna()==False]
    df_target = df_cleaned[target]
    df_cleaned = df_cleaned.drop(unimportant+cannot_be_influenced_directly+correlated+not_a_target+[target], axis=1)
    df_cleaned_transformed = one_hot_encode_string_and_impute(df_cleaned)
    return [df_cleaned]+ train_test_split(df_cleaned_transformed, df_target, random_state=42)

def auto_ML_run_regression(X_train, y_train,X_test, y_test):
    tpot =  TPOTRegressor(generations=5, population_size=20, verbosity=2,config_dict='TPOT light')
    tpot.fit(X_train, y_train)
    best_reg = tpot.fitted_pipeline_
    pickle.dump(best_reg, open('tpot_model.pkl', 'wb'))
    #print(tpot.score(X_test, y_test))
    y_pred = best_reg.predict(X_test)
    return y_pred, best_reg

def auto_ML_plot_results(target,y_test,y_pred):
    plt.close('all')
    plt.plot(np.linspace(y_test.min(),y_test.max(),10),np.linspace(y_test.min(),y_test.max(),10),c='black')
    plt.scatter(y_test,y_pred, c='blue')
    plt.xlabel('True '+ target)
    plt.ylabel('Predicted '+ target)
    plt.title(r'MAE = {0:.2f}, $r^2$ = {1:.2f}'.format(mean_absolute_error(y_test,y_pred), r2_score(y_test,y_pred)))
    plt.savefig('Performance.png', dpi=300)
    plt.show()

def auto_ML_explore_parameters(df_cleaned,best_reg,target,N=10**4,N_best=10):
    df_explore = pd.DataFrame(np.zeros([N,df_cleaned.shape[1]])*np.nan,columns = df_cleaned.keys())
    for key in df_cleaned.dtypes[(df_cleaned.dtypes=='float')].index:
        df_explore[key] = np.random.uniform(df_cleaned[key].min(),df_cleaned[key].max(), N)
    for key in df_cleaned.dtypes[(df_cleaned.dtypes=='object')|(df_cleaned.dtypes=='int')].index:
        df_explore[key] = np.concatenate([np.array(list(set(df_cleaned[key]))),np.random.choice(list(set(df_cleaned[key])), N-len(set(df_cleaned[key])))])
    df_explore_transformed = df_explore.copy()
    for key in df_explore.dtypes[(df_explore.dtypes=='object')].index:
        df_temp =  pd.get_dummies(df_explore[key])
        #print(key,df_temp.shape[1])
        df_explore_transformed.drop(key, axis=1,inplace=True)
        df_explore_transformed[df_temp.keys()] = df_temp
    explore_pred = best_reg.predict(df_explore_transformed)
    best_pred_ind = explore_pred.argsort()[-10:][::-1]
    df_pred = df_explore_transformed.iloc[best_pred_ind]
    df_pred['Pred '+ target] = explore_pred[best_pred_ind]
    print(df_pred)
    df_pred.to_excel('best_candidated.xlsx')
