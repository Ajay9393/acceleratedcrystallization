 -*- coding: utf-8 -*-
"""
Created on Wed Jan  9 15:25:03 2019

@author: Jeffrey
"""
#cluster_num = pick_pl_cluster(wv_est, sat, brightness,test)
#test = rank_images(data_clustered, cluster_num, data)
#plot_all_for_image(data, data_clustered, num_clusters, cluster_num, wv_est, 98)
import numpy as np
import pandas as pd
from skimage.io import imread_collection
from matplotlib import pyplot as plt
import os
import imageio

def k_means(data, nk, max_iter = 100):
    
    """K means algorithm to cluster image by colour with uniform initilization of centroids"""
    # K means algorithm to cluster image colour
    # data: shape [# of images, # of pixels, RGB]
    # nk: number of clusters
    # max_iter: number of iterations per image the algorithm performs before stopping
    
    RGB_max = 255.
    
    # Prepare data
    data_set = np.reshape(data, data.shape + (1,))
    
    # Uniformly initialize centroids throughout the colourspace
    centroid_base = np.arange(nk)
    centroids = np.zeros((2,data.shape[0],1,data.shape[2],nk))
    centroids[0] = (np.ones((data.shape[0],1,data.shape[2],nk)) +  centroid_base[np.newaxis,np.newaxis,np.newaxis,:]) * RGB_max/nk
    centroids[1] = np.ones_like(centroids[0])
    
    
    for i in range(max_iter):
        cluster = np.argmin(np.sum((data_set - centroids[0]) ** 2, axis=2), axis=2)
        
        for j in range(nk):
 
            #################################################################
            # This code stops calculations for centroids that are constant between iterations but is buggy
#
#            centroid_mask = (cluster == j) * np.logical_not((np.all(centroids[0] == centroids[2], axis=(1,2,3))))[:,np.newaxis]
#            centroids[2] = centroids[1]
            #################################################################
            
            centroid_mask = (cluster == j)
            cluster_counts = np.count_nonzero(centroid_mask, axis=1)[:,np.newaxis]
            zero_cond = (cluster_counts==0)

            centroids_j = (np.sum(data * centroid_mask[..., np.newaxis], axis=1 ) / (cluster_counts + zero_cond)) * np.logical_not(zero_cond)
            centroids[0, ..., j] = centroids_j.reshape((centroids_j.shape[0],1,centroids_j.shape[1]))
                
        if np.all(centroids[0] == centroids[1]):
            break
        
        centroids[1] = centroids[0]
        print('Iteration ' + str(i) + ' complete.')

    return (cluster, centroids[0,:,0])

def im_cut_out_circle(image, o_rel_x, o_rel_y, r_rel, value=225):
    
    """Sets all data outside a circle in an image to a value"""
    # im: set of images
    # o_rel: position of the origin relative to the x and y axes (along the diagonal)
    # r_rel: position of the origin relative to the x axis
    # value: the value set to the points outside the circle
    
    origin = np.asarray( (o_rel_y * image.shape[1], o_rel_x * image.shape[2]) )
    radius = r_rel * image.shape[2]
    x,y = np.ogrid[-origin[0]:image.shape[1]-origin[0], -origin[1]:image.shape[2]-origin[1]]
    mask = x*x + y*y <= radius*radius
    _, mask_b = np.broadcast_arrays(image[...,0], mask[None, ...])
    image[~mask_b[:,:,:]] = value
    
    return image

def im_divide(im_n, im_d):
    """Image division of 2 layers"""
    blend = np.minimum(im_n/im_d, 1.0)
    return blend

def im_subtract(im_n, im_d):
    """Image subtraction of 2 layers"""
    blend = np.abs(im_n - im_d)
    return blend

def get_hsv_to_wavelength():
    
    """Generates the HSV/HSB hues to wavelength curve"""
    
    curve = np.zeros(360)
    curve[265:360] = np.zeros(95)
    curve[240:265] = np.linspace(440,420,25)
    curve[180:240] = np.linspace(490,440,60)
    curve[120:180] = np.linspace(510,490,60)
    curve[60:120] = np.linspace(580,510,60)
    curve[0:60] = np.linspace(645,580,60)
    
    return curve

def wavelength_estimate(RGB, black_limit=50, white_limit=220):
    
    """Estimates wavelength from an RGB value"""
    
    # black_limit: Value that if all RGB values are below it, image is considered black
    # white_limit: Value that if all RGB values are above it, image is considered white
    
    # First find the hue from the RGB value
    RGB = RGB.astype(np.float32)
    C_max = np.amax(RGB,axis=2)
    C_max_i = np.argmax(RGB,axis=2)
    C_min = np.amin(RGB,axis=2)
    
    hues = np.zeros(RGB.shape)
    
    # True if RGB colour below black limit, above white limit, or unsaturated
    zero_cond = np.any(np.concatenate(((C_max <= black_limit)[:,np.newaxis], (C_min >= white_limit)[:,np.newaxis], np.equal(C_max,C_min)[:,np.newaxis]), axis=1), axis=1)
    
    hues[:,:,0] = 60 * ( np.nan_to_num((RGB[:,:,1] - RGB[:,:,2])/(C_max - C_min + zero_cond)) % 6 ) * (np.logical_not(zero_cond))*1 + zero_cond*300
    hues[:,:,1] = 60 * ( np.nan_to_num((RGB[:,:,2] - RGB[:,:,0])/(C_max - C_min + zero_cond)) + 2 ) * (np.logical_not(zero_cond))*1 + zero_cond*300
    hues[:,:,2] = 60 * ( np.nan_to_num((RGB[:,:,0] - RGB[:,:,1])/(C_max - C_min + zero_cond)) + 4 ) * (np.logical_not(zero_cond))*1 + zero_cond*300
    
    idx = np.mgrid[0:hues.shape[0],0:hues.shape[1]]
    hues = hues[idx[0], idx[1], C_max_i]
    
    # Convert the hue back to approximate wavelength
    hue_i = np.arange(0,360,0.01)
    hsv_to_wv = get_hsv_to_wavelength()
    hsv_to_wv_interp = np.interp(hue_i, np.arange(0,360), hsv_to_wv)
    hsv_to_wv_interp[hsv_to_wv_interp < 420] = 0
    
    wavelengths = hsv_to_wv_interp[ np.searchsorted(hue_i, hues) ]
    saturation = np.divide(C_max-C_min, C_max, out=np.zeros_like(C_max), where = C_max != 0)
    brightness = C_max/255
    
    return wavelengths, hues, saturation, brightness

def im_hist(im):
    
    """Generates histograms of wavelengths for each pixel in a given image"""
    if len(im.shape) == 2:
        im = im[np.newaxis]
        
    wv_est,_,_,brt = wavelength_estimate(im)
    
    hist_wv = []
    hist_brt = []
    for i in range(im.shape[0]):
        
        # Slice only good pixels
        brt_i = brt[i,np.not_equal(wv_est[i],0)]
        wv_est_i = wv_est[i,np.not_equal(wv_est[i],0)]
        
        # Count values
        wavelengths = np.round(wv_est_i, decimals=1)
        unique, counts = np.unique(wavelengths, return_counts=True)
        hist_wv.append(np.vstack((unique, counts)))
        
        brightness = np.round(brt_i, decimals=2)
        unique, counts = np.unique(brightness, return_counts=True)
        hist_brt.append(np.vstack((unique, counts)))
    
    return hist_wv, hist_brt

def pick_pl_cluster(wv_est, saturation, brightness, cluster_count):
    
    pixels_significant = 500
    
    pixel_num_metric = cluster_count > pixels_significant
    wavelength_metric = np.logical_not(wv_est == 0)
    metric = pixel_num_metric*wavelength_metric*saturation
    
    empty_clusters = -1*np.all(metric == 0, axis=1)
    best_clusters = np.argmax(metric, axis=1) + empty_clusters
    
    cluster_num = best_clusters
    
    return cluster_num

def find_wv_of_images(images, num_clusters=6):
    
    circle_x = 0.439
    circle_y = 0.523
    circle_r = 0.35
    set_value = 0
    
    im_cutout = im_cut_out_circle(images,circle_x,circle_y,circle_r,set_value).astype('uint8')
    data = im_cutout.reshape((-1,im_cutout.shape[1]*im_cutout.shape[2],im_cutout.shape[3]))
    (data_clustered, centroids) = k_means(data, num_clusters)
    wv_est, hues, sat, brightness = wavelength_estimate(centroids.reshape((-1,3,num_clusters)).transpose((0,2,1)))
    
    cluster_count = bincount2D_vectorized(data_clustered)
    cluster_num = pick_pl_cluster(wv_est, sat, brightness, cluster_count)
    
    return wv_est[np.arange(wv_est.shape[0]), cluster_num]

def bincount2D_vectorized(a):    
    N = a.max()+1
    a_offs = a + np.arange(a.shape[0])[:,None]*N
    return np.bincount(a_offs.ravel(), minlength=a.shape[0]*N).reshape(-1,N)    

def plot_cluster_im(im_clustered, num_clusters):
    plt.figure()
    plt.imshow(im_clustered,cmap=plt.cm.get_cmap('gist_ncar', num_clusters))
    plt.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
    plt.tick_params(axis='y', which='both', left=False, right=False, labelleft=False) 
    cbar = plt.colorbar(ticks=range(num_clusters))
    plt.clim(-0.5, num_clusters-0.5)
    cbar.set_label('cluster number')

def parse_hist(hist, plot=True, plot_type='wv'):

    """Parses through a histogram and finds the average value as well as the standard deviation"""
    
    # Convert histagram into numpy array
    avg = np.zeros(len(hist))
    med = np.zeros_like(avg)
    std = np.zeros_like(avg)
    maximum = np.zeros_like(avg)
    
    for i in range(len(hist)):
    
        keys = hist[i][0]
        vals = hist[i][1]

        if keys.size > 0:
         
            # Compute average
            avg[i] = np.sum(keys*vals)/np.sum(vals)
            med[i] = np.median(keys)
            
            # Compute standard deviation
            var_sum = np.dot((keys - avg[i])**2, vals)
            num_of_entries = np.sum(vals)
            var = var_sum/(num_of_entries-1)
            std[i] = np.sqrt(var)
            maximum[i] = np.max(keys)
            
        else:
            avg[i] = 0
            med[i] = 0
            std[i] = 0
            maximum[i] = 0
    
    if plot:
        
        if plot_type == 'brt':
            x_axis = 'Brightness'
            legend = '$B_{med}=%.2f$\n$B_{avg}=%.2f$\n$\sigma=%.2f$'
            width = 0.01
        else:
            x_axis = 'Wavelength ($\mathrm{nm}$)'
            legend = '$\lambda_{med}=%.2fnm$\n$\lambda_{avg}=%.2fnm$\n$\sigma=%.2fnm$'
            width = 0.1
        
        f = plt.figure()
        ax = f.add_subplot(111)
        plt.bar(keys, vals, color='g', width=width)
        textstr = legend % (med,avg,std)
        ax.set_xlabel(x_axis)
        ax.set_ylabel('Counts')
        ax.text(0.95, 0.95, textstr, transform=ax.transAxes, fontsize=14,
            verticalalignment='top', horizontalalignment='right')
    
    return avg, med, std, maximum

def plot_all_for_image(originals, clustered, num_clusters, chosen_clusters, wv_est, im_number):
    
    originals_f = originals.reshape((-1,150,200,3))
    clustered_f = clustered.reshape((-1,150,200))
    
    # Plot images
    plt.figure()
    plt.imshow(originals_f[im_number])
    plot_cluster_im(clustered_f[im_number], num_clusters)
    print("Chosen cluster: " + str(chosen_clusters[im_number]))
    
#    hist_whole_im = im_hist(data[0].reshape(-1,3))
    im_just_crystal = np.equal(clustered[im_number], chosen_clusters[im_number])[:,np.newaxis]*originals[im_number]
    hist_wv, hist_brt = im_hist(im_just_crystal.reshape(-1,3))
    parse_hist(hist_wv, plot_type='wv')
    parse_hist(hist_brt, plot_type='brt')

def parse_images(data_clustered, cluster_num, originals):
    im_cluster_pixels = np.equal(data_clustered, cluster_num[:,np.newaxis])
    data_masked = im_cluster_pixels[..., np.newaxis] * originals
    hist_wv, hist_brt = im_hist(data_masked)
    wv_avg, wv_med, wv_std, _ = parse_hist(hist_wv, plot=False)
    brt_avg, brt_med, brt_std, brt_max = parse_hist(hist_brt, plot=False)
    
    return (wv_avg, wv_med, wv_std), (brt_avg, brt_med, brt_std, brt_max)

def rank_images(data_clustered, cluster_num, originals):
    wv, brt = parse_images(data_clustered, cluster_num, originals)
    image_info = np.hstack((np.arange(cluster_num.size)[:,np.newaxis], brt[0][:,np.newaxis], wv[0][:,np.newaxis]))
    idxs = np.argsort(brt[0])
    
    return np.flipud(image_info[idxs])
    
if __name__== "__main__":
      
#    # Load images
#    dark = iio.imread('new.jpg').astype('float32')
#    pl = iio.imread('new.jpg').astype('float32')
#    im = np.zeros((2,pl.shape[0],pl.shape[1],pl.shape[2]))
#    im[0] = pl
#    im[1] = pl
#    test = find_wv_of_images(im)
##    im_noise_removed = im_subtract(pl,dark)
##    im = im_cut_out_circle(pl,0.473,0.34375).astype('uint8')
#    im = im_cut_out_circle(im,0.439,0.523,0.35).astype('uint8')
#    
#    # Format data for pl detection algorithm
#    data_new = np.zeros((2,pl.shape[0]*pl.shape[1],pl.shape[2]))
#    data_new[0] = im[0].reshape((1,pl.shape[0]*pl.shape[1],pl.shape[2]))
#    data_new[1] = im[0].reshape((1,pl.shape[0]*pl.shape[1],pl.shape[2]))
    
    # Load images
    path = 'E:/Jeff/Robotics Experiments/1208/th/p17/'
#    path = 'C:/Users/Jeffrey/Desktop/Photos/*.jpg'
    images = np.stack([imageio.imread(path + im) for im in os.listdir(path)], axis=0)
#    im_circle = im_cut_out_circle(images,0.439,0.523,0.35,value=0).astype('uint8')
#    data = im_circle.reshape((im_circle.shape[0],im_circle.shape[1]*im_circle.shape[2],im_circle.shape[3]))
    data = images.reshape((images.shape[0],images.shape[1]*images.shape[2],images.shape[3]))
    # Cluster the image
    num_clusters = 5

    (data_clustered, centroids) = k_means(data, num_clusters)
    wv_est, hues, sat, brightness = wavelength_estimate(centroids.reshape((-1,3,num_clusters)).transpose((0,2,1)))
    
    cluster_count = bincount2D_vectorized(data_clustered)
    cluster_num = pick_pl_cluster(wv_est, sat, brightness, cluster_count)
    
#    im_clustered = data_clustered[0].reshape((1,150,200))
#    plot_cluster_im(im_clustered, num_clusters)
#    
#    # Calculate histogram of pixel colours in the image and a specific cluster
#    cluster_num = pick_pl_cluster(wv_est, sat, brightness)
#    
#    hist_whole_im = im_hist(data[0].reshape(-1,3))
#    im_just_crystal = np.equal(data_clustered[0], cluster_num[0])[:,np.newaxis]*data[0]
#    hist_cluster = im_hist(im_just_crystal.reshape(-1,3))
#    del hist_cluster[0] # Remove discarded points
#    plot_hist(hist_cluster, wv_est[0,cluster_num[0]])
    
    plot_all_for_image(data, data_clustered, num_clusters, cluster_num, wv_est, 2)
