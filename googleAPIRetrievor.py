# -*- coding: utf-8 -*-
"""
Created on Tue Sep 04 11:30:03 2018

@author: ak4jo
"""

import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
import numpy as np
import pickle

##Name of worksheet for which the parameters should be extracted
worksheet_title = '2018-07-10-1'

##Get access to the google sheets document
scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']


##Note: .json file needs to be in same folder as this code
creds = ServiceAccountCredentials.from_json_keyfile_name('Robotic Perovskites-598ce0c18724.json', scope)
gc = gspread.authorize(creds)
sheet = gc.open("Robotic_Feature_Template").worksheet(worksheet_title)

#Extract all cells into a dataframe
df = pd.DataFrame(sheet.get_all_records())

##Formatting: get volumes of three drops and remove column from array
drop_volumes = [int(x) for x in df.loc[:]['Drop Volumes'] if x !=""]
df.drop('Drop Volumes', axis = 1, inplace = True)

drop_array = np.reshape(np.tile(drop_volumes,len(df.index)), [len(df.index)*3,1])

##Create array that duplicates each row (well number) 3 times, and adds the corresponding drop volume to it
##This takes the 96 wells and expands it to 288 subwells
combined = np.concatenate((np.repeat(df.values,3,axis=0),drop_array),axis = 1)
columns_list = [x for x in df.columns]
columns_list.append('Drop Volumes')
compiled_df = pd.DataFrame(combined, columns = columns_list)

compiled_df.to_pickle('features.pickle')










